package main

import (
	"io/ioutil"
	"log"

	"fyne.io/fyne/app"
	"fyne.io/fyne/widget"
	"github.com/go-vgo/robotgo"
)

func main() {
	a := app.New()
	w := a.NewWindow("Hello")

	hello := widget.NewLabel("Hello Fyne!")
	w.SetContent(widget.NewVBox(
		hello,
		widget.NewButton("Hi!", func() {
			bit := robotgo.CaptureScreen()
			bb := robotgo.ToBitmapBytes(bit)
			log.Println(ioutil.WriteFile("demo.png", bb, 0777))
			hello.SetText("Welcome :)")
		}),
	))

	w.ShowAndRun()
}
