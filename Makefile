# 編譯
build: vendor
	go build -mod vendor

# 依賴套件
vendor: go.sum
	rm go.sum
	GOPROXY= go mod tidy
	GOPROXY= go mod vendor
	# 因為 go mod vendor 不會複製C語言檔案，所以特別手動複製原程式碼到vendor裡面
	rm -r vendor/github.com/go-vgo/robotgo || exit 0
	rm -r vendor/github.com/robotn/gohook || exit 0
	cp -R vendor_c/github.com/go-vgo/robotgo vendor/github.com/go-vgo/robotgo
	cp -R vendor_c/github.com/robotn/gohook vendor/github.com/robotn/gohook

# Go套件管理
go.mod:
	go mod init gomo

go.sum: go.mod
	go mod tidy
